package com.kaffka.zooplus.models;

/**
 * Created by kaffka on 4/21/2017.
 */

public interface ProductModel {

    String getProductName();

    String getProductPrice();

    String getPriceCurrency();

    String getImageUrl();

    String getProductId();
}
