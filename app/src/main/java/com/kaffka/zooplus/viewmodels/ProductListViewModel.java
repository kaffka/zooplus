package com.kaffka.zooplus.viewmodels;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.view.View;

import com.kaffka.zooplus.activities.CheckoutActivity;

/**
 * Created by kaffka on 4/21/2017.
 */

public class ProductListViewModel extends BaseObservable {

    private boolean isLoading;

    public ProductListViewModel(boolean isLoading) {
        this.isLoading = isLoading;
    }

    public int getProgresVisibility() {
        return isLoading ? View.VISIBLE : View.GONE;
    }

    public int getCheckoutButtonVisibility() {
        return isLoading ? View.GONE : View.VISIBLE;
    }

    public View.OnClickListener checkout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), CheckoutActivity.class);
                v.getContext().startActivity(i);
            }
        };
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
