package com.kaffka.zooplus.viewmodels;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.kaffka.zooplus.R;
import com.kaffka.zooplus.models.ProductModel;
import com.squareup.picasso.Picasso;

/**
 * Created by kaffka on 4/21/2017.
 */

public class ProductItemViewModel extends BaseObservable {

    private ProductModel productModel;

    public ProductItemViewModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    public String getProductName() {
        return productModel.getProductName();
    }

    public String getProductPrice() {
        return productModel.getProductPrice() + " " + productModel.getPriceCurrency();
    }

    public String getProductId() {
        return productModel.getProductId();
    }

    @BindingAdapter({"bind:productId"})
    public static void loadImage(ImageView view, String productId) {

        int drawableResourceId = view.getContext().getResources().getIdentifier("z" + productId + ".png", "drawable", view.getContext().getPackageName());

        Picasso.with(view.getContext())
                .load(drawableResourceId)
                .placeholder(R.drawable.ic_placeholder)
                .into(view);
    }

    public View.OnClickListener addToCart() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), R.string.added_to_cart, Toast.LENGTH_SHORT).show();
            }
        };
    }
}
