package com.kaffka.zooplus.application;

import android.app.Application;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kaffka on 4/21/2017.
 */

public class ZooplusApplication extends Application {
    private static ZooplusApplication instance;
    private Retrofit defaultRetrofit;
    public static final String BASE_URL = "http://10.0.2.2:3000/";

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }

    public static ZooplusApplication getApplicationInstance() {
        return instance;
    }

    public Retrofit getDefaultRetrofit() {
        return configureRetrofit(BASE_URL);
    }

    @NonNull
    private OkHttpClient getOkHttpClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    @NonNull
    private Retrofit configureRetrofit(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
