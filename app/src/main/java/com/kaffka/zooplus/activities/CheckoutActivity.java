package com.kaffka.zooplus.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.kaffka.zooplus.R;
import com.kaffka.zooplus.application.ZooplusApplication;
import com.kaffka.zooplus.repository.PutOrderRequest;
import com.kaffka.zooplus.repository.PutOrderResponse;
import com.kaffka.zooplus.repository.ZooplusApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kaffka on 4/21/2017.
 */

public class CheckoutActivity extends AppCompatActivity {

    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        progressBar = (ProgressBar) findViewById(R.id.activity_checkout_progress_bar);
    }


    public void onPlaceOrderClick(View v) {
        placeOrder();
        progressBar.setVisibility(View.VISIBLE);
    }


    private void placeOrder() {
        ZooplusApplication.getApplicationInstance().getDefaultRetrofit().create(ZooplusApi.class).putOrder(new PutOrderRequest()).enqueue(new Callback<PutOrderResponse>() {
            @Override
            public void onResponse(Call<PutOrderResponse> call, Response<PutOrderResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(CheckoutActivity.this,R.string.order_success,Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<PutOrderResponse> call, Throwable t) {
                Toast.makeText(CheckoutActivity.this, R.string.unknown_error, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}
