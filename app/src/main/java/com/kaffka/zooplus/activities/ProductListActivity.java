package com.kaffka.zooplus.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import com.kaffka.zooplus.R;
import com.kaffka.zooplus.adapters.ProductListAdapter;
import com.kaffka.zooplus.application.ZooplusApplication;
import com.kaffka.zooplus.databinding.ActivityProductListBinding;
import com.kaffka.zooplus.models.ProductModel;
import com.kaffka.zooplus.repository.GetProductResponse;
import com.kaffka.zooplus.repository.GetProductResponseWrapper;
import com.kaffka.zooplus.repository.ZooplusApi;
import com.kaffka.zooplus.viewmodels.ProductListViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kaffka on 4/21/2017.
 */

public class ProductListActivity extends AppCompatActivity {

    ActivityProductListBinding activityProductListBinding;
    ProductListAdapter productListAdapter;
    List<ProductModel> productList;
    ProductListViewModel productListViewModel = new ProductListViewModel(false);
    List<ProductModel> checkoutList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityProductListBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_list);
        activityProductListBinding.setViewmodel(productListViewModel);
        initRecyclerView();
        initToolbar();
        getProductList();
    }

    private void initRecyclerView() {
        activityProductListBinding.activityProductListList.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        activityProductListBinding.activityProductListList.setLayoutManager(mLayoutManager);
        productList = new ArrayList<>();
        productListAdapter = new ProductListAdapter(productList);
        activityProductListBinding.activityProductListList.setAdapter(productListAdapter);
    }

    private void initToolbar() {
        setSupportActionBar(activityProductListBinding.activityProductListToolbar);
    }

    private void getProductList() {
        ZooplusApplication.getApplicationInstance().getDefaultRetrofit().create(ZooplusApi.class).getProducts().enqueue(new Callback<GetProductResponseWrapper>() {
            @Override
            public void onResponse(Call<GetProductResponseWrapper> call, Response<GetProductResponseWrapper> response) {
                if (response.isSuccessful()) {
                    addResultsToList(response.body().getProducts());
                }
            }

            @Override
            public void onFailure(Call<GetProductResponseWrapper> call, Throwable t) {
                Toast.makeText(ProductListActivity.this,R.string.unknown_error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addResultsToList(List<GetProductResponse> productList) {
        this.productList.clear();
        this.productList.addAll(productList);
        productListAdapter.notifyDataSetChanged();
        productListViewModel.setLoading(false);
        activityProductListBinding.setViewmodel(productListViewModel);
    }
}
