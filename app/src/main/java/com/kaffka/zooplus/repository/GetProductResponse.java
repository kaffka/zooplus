package com.kaffka.zooplus.repository;

import com.kaffka.zooplus.models.ProductModel;

/**
 * Created by kaffka on 4/21/2017.
 */

public class GetProductResponse implements ProductModel {
    private String id;
    private String name;
    private String price;
    private String currency;
    private String image_base_url;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public String getImage_base_url() {
        return image_base_url;
    }

    @Override
    public String getProductName() {
        return getName();
    }

    @Override
    public String getProductPrice() {
        return getPrice();
    }

    @Override
    public String getPriceCurrency() {
        return getCurrency();
    }

    @Override
    public String getImageUrl() {
        return getImage_base_url();
    }

    @Override
    public String getProductId() {
        return getProductId();
    }
}
