package com.kaffka.zooplus.repository;

import java.util.List;

/**
 * Created by kaffka on 4/21/2017.
 */

public class GetProductResponseWrapper {
    List<GetProductResponse> products;

    public List<GetProductResponse> getProducts() {
        return products;
    }
}
