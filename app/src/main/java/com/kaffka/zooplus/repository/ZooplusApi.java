package com.kaffka.zooplus.repository;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;

/**
 * Created by kaffka on 4/21/2017.
 */

public interface ZooplusApi {
    @GET("/products")
    Call<GetProductResponseWrapper> getProducts();

    @PUT("/orders")
    Call<PutOrderResponse> putOrder(@Body PutOrderRequest putOrderRequest);
}
