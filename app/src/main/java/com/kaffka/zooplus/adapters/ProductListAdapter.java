package com.kaffka.zooplus.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kaffka.zooplus.R;
import com.kaffka.zooplus.databinding.ProductItemBinding;
import com.kaffka.zooplus.models.ProductModel;
import com.kaffka.zooplus.viewmodels.ProductItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaffka on 4/21/2017.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductItemViewHolder> {

    private List<ProductModel> productList;
    private List<ProductModel> checkoutList;

    public ProductListAdapter(List<ProductModel> productList) {
        this.productList = productList;
        checkoutList = new ArrayList<>();
    }

    @Override
    public ProductItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductListAdapter.ProductItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductItemViewHolder holder, final int position) {
        holder.productItemBinding.setViewmodel(new ProductItemViewModel(productList.get(position)));
        holder.productItemBinding.productItemAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkoutList.add(productList.get(position));
                Toast.makeText(v.getContext(),"Product successfully added", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public static class ProductItemViewHolder extends RecyclerView.ViewHolder {

        public final ProductItemBinding productItemBinding;

        public ProductItemViewHolder(View v) {
            super(v);
            productItemBinding = ProductItemBinding.bind(v);
        }
    }

    public List<ProductModel> getCheckoutList(){
        return checkoutList;
    }
}
