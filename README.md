# ZooPlus Test App
A simple app that consumes a local node.js api. below are a few observations:


- The app has no unit/intrumented test due to the imposed time constrains


- The UI in the checkout screen can easily be improved to show a list of the itens added to the bascket


- The user feedback shown when a network operations is happening can also be largely improved


- I could't use the provided image resources, I had a bug when rendering them in the `ProductItemViewModel` class and opted to "finish" the checkout screen and meet the time constrain

## Getting started
Clone the repository and import the project in Android Studio.
You will need to configure the `BASE_URL` in the `ZooplusApplication` in order to test the app. If you are running the server locally you can test the app in an Android emulator and use the base url as is `http://10.0.2.2:3000/`. 

## Dependencies
The depdendencies for the project are described in the Gradle script `build.gradle` of the app, just sync the project in Android Studio and you are ready to go :)

List of third party libraries used in the project so far


- [Retrofit/OKhttp](http://square.github.io/retrofit/) - A type-safe REST client for Android and Java


- [Gson](https://github.com/google/gson) - Gson is a Java library that can be used to convert Java Objects into their JSON representation.

- [Picasso](http://square.github.io/picasso/) - A powerful image downloading and caching library for Android


## Running it
You can [build and run](https://developer.android.com/tools/building/building-studio.html) the project using the Android Studio/Gradle. You need an [android emulator](http://developer.android.com/tools/devices/emulator.html) or a [real device](http://developer.android.com/tools/device.html) to test the build.


## Screenshots
![](app.gif)